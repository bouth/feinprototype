package com.bouthaina.feinprototype

import android.app.Application
import com.google.firebase.FirebaseApp

class BaseApplication : Application() {

        override fun onCreate() {
            super.onCreate()
            instance = this
            FirebaseApp.initializeApp(this)

        }

        companion object {
            lateinit var instance: BaseApplication
                private set
        }
    }